<?php

/**
 * Filter handler which uses list-fields as options.
 *
 * @ingroup views_filter_handlers
 */
class admin_blocks_view_handler_filter_block_theme extends views_handler_filter_many_to_one {

  function init(&$view, &$options) {
    parent::init($view, $options);
    // Migrate the settings from the old filter_in_operator values to filter_many_to_one.
    if ($this->options['operator'] == 'in') {
      $this->options['operator'] = 'or';
    }
    if ($this->options['operator'] == 'not in') {
      $this->options['operator'] = 'not';
    }
    $this->operator = $this->options['operator'];
  }


  function get_value_options() {
    $this->value_options = array();
    $themes = list_themes();

    foreach ($themes as $theme_name => $theme) {
      $this->value_options[$theme_name] = $theme->info['name'];
    }
  }
}
