<?php

class admin_blocks_view_complex_join extends views_join
{

  function construct($table = null, $left_table = null, $left_field = null, $field = null, $extra = array(), $type = 'LEFT') {
    parent::construct($table, $left_table, $left_field, $field, $extra, $type);
  }

  function build_join($select_query, $table, $view_query)
  {
    $this->extra = 'foo.bar = baz.boing';
    parent::build_join($select_query, $table, $view_query);
  }
}