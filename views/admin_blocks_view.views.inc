<?php

/**
 * @file
 * Alter view data for block object.
 */

/**
 * Implements hook_views_data().
 */
function admin_blocks_view_views_data() {
  $data['block']['table']['group'] = t('Core blocks');

  $data['block']['table']['base'] = array(
    'field' => 'bid',
    'title' => t('Core blocks'),
    'help' => t('Block table contains all custom and module\'s blocks.'),
    'weight' => 0,
  );

  $data['block']['bid'] = array(
    'title' => t('Bid'),
    'help' => t('The block id.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'bid',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['block']['title'] = array(
    'title' => t('Block title'),
    'help' => t('The blocks\s title (usually it is empty).'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['block']['module'] = array(
    'title' => t('Block module'),
    'help' => t('The blocks\s module'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['block']['delta'] = array(
    'title' => t('Block delta'),
    'help' => t('The blocks\s delta'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['block']['theme'] = array(
    'title' => t('Block theme'),
    'help' => t('The blocks\s theme'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'admin_blocks_view_handler_filter_block_theme',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['block']['status'] = array(
    'title' => t('Block status'),
    'help' => t('If block is enabled.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['block']['weight'] = array(
    'title' => t('Block weight'),
    'help' => t('The blocks\s weight in particlar region'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['block']['region'] = array(
    'title' => t('Block region'),
    'help' => t('The blocks\s region'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['block']['region'] = array(
    'title' => t('Block region'),
    'help' => t('The blocks\s region'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['block']['operations'] = array(
    'title' => t('Block operations'),
    'help' => t('Edit and delete links.'),
    'field' => array(
      'handler' => 'admin_blocks_view_handler_field_block_operations',
    ),
  );

  $data['admin_blocks_titles']['table']['group'] = t('Block titles');

  $data['admin_blocks_titles']['table']['join'] = array(
    'block' => array(
      'left_field' => 'delta',
      'field' => 'delta',
//      'handler' => 'admin_blocks_view_complex_join',
      'extra' => 'block.module = admin_blocks_titles.module'
    ),
  );

  $data['admin_blocks_titles']['title'] = array(
    'title' => t('Block administrative title'),
    'help' => t('The administrative title'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}

/**
 * Implementation of hook_views_plugins().
 */
function admin_blocks_view_views_plugins() {
  $plugin = array();

  $plugin['query']['admin_blocks_view_plugin_query'] = array(
    'title' => t('Admin Blocks Query'),
    'help' => t('Admin Blocks query object.'),
    'handler' => 'admin_blocks_view_plugin_query',
  );

  return $plugin;
}

/**
 * Implements hook_views_default_views().
 */
function admin_blocks_view_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'core_blocks_example';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'block';
  $view->human_name = 'Core blocks example';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'bid' => 'bid',
    'info' => 'info',
    'title' => 'title',
    'module' => 'module',
    'delta' => 'delta',
    'status' => 'status',
    'region' => 'region',
    'theme' => 'theme',
    'weight' => 'weight',
    'operations' => 'operations',
  );
  $handler->display->display_options['style_options']['default'] = 'bid';
  $handler->display->display_options['style_options']['info'] = array(
    'bid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'info' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'module' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'delta' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'status' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'region' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'theme' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'weight' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'operations' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Core blocks: Bid */
  $handler->display->display_options['fields']['bid']['id'] = 'bid';
  $handler->display->display_options['fields']['bid']['table'] = 'block';
  $handler->display->display_options['fields']['bid']['field'] = 'bid';
  /* Field: Block titles: Block administrative title */
  $handler->display->display_options['fields']['title_1']['id'] = 'title_1';
  $handler->display->display_options['fields']['title_1']['table'] = 'admin_blocks_titles';
  $handler->display->display_options['fields']['title_1']['field'] = 'title';
  /* Field: Core blocks: Block title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'block';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Block subject';
  /* Field: Core blocks: Block module */
  $handler->display->display_options['fields']['module']['id'] = 'module';
  $handler->display->display_options['fields']['module']['table'] = 'block';
  $handler->display->display_options['fields']['module']['field'] = 'module';
  /* Field: Core blocks: Block delta */
  $handler->display->display_options['fields']['delta']['id'] = 'delta';
  $handler->display->display_options['fields']['delta']['table'] = 'block';
  $handler->display->display_options['fields']['delta']['field'] = 'delta';
  /* Field: Core blocks: Block status */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'block';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  $handler->display->display_options['fields']['status']['type'] = 'unicode-yes-no';
  $handler->display->display_options['fields']['status']['not'] = 0;
  /* Field: Core blocks: Block region */
  $handler->display->display_options['fields']['region']['id'] = 'region';
  $handler->display->display_options['fields']['region']['table'] = 'block';
  $handler->display->display_options['fields']['region']['field'] = 'region';
  /* Field: Core blocks: Block theme */
  $handler->display->display_options['fields']['theme']['id'] = 'theme';
  $handler->display->display_options['fields']['theme']['table'] = 'block';
  $handler->display->display_options['fields']['theme']['field'] = 'theme';
  /* Field: Core blocks: Block weight */
  $handler->display->display_options['fields']['weight']['id'] = 'weight';
  $handler->display->display_options['fields']['weight']['table'] = 'block';
  $handler->display->display_options['fields']['weight']['field'] = 'weight';
  /* Field: Core blocks: Block operations */
  $handler->display->display_options['fields']['operations']['id'] = 'operations';
  $handler->display->display_options['fields']['operations']['table'] = 'block';
  $handler->display->display_options['fields']['operations']['field'] = 'operations';
  /* Filter criterion: Block titles: Block administrative title */
  $handler->display->display_options['filters']['title_1']['id'] = 'title_1';
  $handler->display->display_options['filters']['title_1']['table'] = 'admin_blocks_titles';
  $handler->display->display_options['filters']['title_1']['field'] = 'title';
  $handler->display->display_options['filters']['title_1']['operator'] = 'contains';
  $handler->display->display_options['filters']['title_1']['group'] = 1;
  $handler->display->display_options['filters']['title_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title_1']['expose']['operator_id'] = 'title_1_op';
  $handler->display->display_options['filters']['title_1']['expose']['label'] = 'Block administrative title';
  $handler->display->display_options['filters']['title_1']['expose']['operator'] = 'title_1_op';
  $handler->display->display_options['filters']['title_1']['expose']['identifier'] = 'title_1';
  $handler->display->display_options['filters']['title_1']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    11 => 0,
    16 => 0,
    21 => 0,
    26 => 0,
    31 => 0,
    36 => 0,
    41 => 0,
    46 => 0,
    51 => 0,
    56 => 0,
  );
  /* Filter criterion: Core blocks: Block title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'block';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['operator'] = 'contains';
  $handler->display->display_options['filters']['title']['group'] = 1;
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Block title';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    11 => 0,
    16 => 0,
    21 => 0,
    26 => 0,
    31 => 0,
    36 => 0,
    41 => 0,
    46 => 0,
    51 => 0,
    56 => 0,
  );
  /* Filter criterion: Core blocks: Bid */
  $handler->display->display_options['filters']['bid']['id'] = 'bid';
  $handler->display->display_options['filters']['bid']['table'] = 'block';
  $handler->display->display_options['filters']['bid']['field'] = 'bid';
  $handler->display->display_options['filters']['bid']['group'] = 1;
  $handler->display->display_options['filters']['bid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['bid']['expose']['operator_id'] = 'bid_op';
  $handler->display->display_options['filters']['bid']['expose']['label'] = 'Bid';
  $handler->display->display_options['filters']['bid']['expose']['operator'] = 'bid_op';
  $handler->display->display_options['filters']['bid']['expose']['identifier'] = 'bid';
  $handler->display->display_options['filters']['bid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    11 => 0,
    16 => 0,
    21 => 0,
    26 => 0,
    31 => 0,
    36 => 0,
    41 => 0,
    46 => 0,
    51 => 0,
    56 => 0,
  );
  /* Filter criterion: Core blocks: Block delta */
  $handler->display->display_options['filters']['delta']['id'] = 'delta';
  $handler->display->display_options['filters']['delta']['table'] = 'block';
  $handler->display->display_options['filters']['delta']['field'] = 'delta';
  $handler->display->display_options['filters']['delta']['operator'] = 'contains';
  $handler->display->display_options['filters']['delta']['group'] = 1;
  $handler->display->display_options['filters']['delta']['exposed'] = TRUE;
  $handler->display->display_options['filters']['delta']['expose']['operator_id'] = 'delta_op';
  $handler->display->display_options['filters']['delta']['expose']['label'] = 'Block delta';
  $handler->display->display_options['filters']['delta']['expose']['operator'] = 'delta_op';
  $handler->display->display_options['filters']['delta']['expose']['identifier'] = 'delta';
  $handler->display->display_options['filters']['delta']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    11 => 0,
    16 => 0,
    21 => 0,
    26 => 0,
    31 => 0,
    36 => 0,
    41 => 0,
    46 => 0,
    51 => 0,
    56 => 0,
  );
  /* Filter criterion: Core blocks: Block module */
  $handler->display->display_options['filters']['module']['id'] = 'module';
  $handler->display->display_options['filters']['module']['table'] = 'block';
  $handler->display->display_options['filters']['module']['field'] = 'module';
  $handler->display->display_options['filters']['module']['operator'] = 'contains';
  $handler->display->display_options['filters']['module']['group'] = 1;
  $handler->display->display_options['filters']['module']['exposed'] = TRUE;
  $handler->display->display_options['filters']['module']['expose']['operator_id'] = 'module_op';
  $handler->display->display_options['filters']['module']['expose']['label'] = 'Block module';
  $handler->display->display_options['filters']['module']['expose']['operator'] = 'module_op';
  $handler->display->display_options['filters']['module']['expose']['identifier'] = 'module';
  $handler->display->display_options['filters']['module']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    11 => 0,
    16 => 0,
    21 => 0,
    26 => 0,
    31 => 0,
    36 => 0,
    41 => 0,
    46 => 0,
    51 => 0,
    56 => 0,
  );
  /* Filter criterion: Core blocks: Block region */
  $handler->display->display_options['filters']['region']['id'] = 'region';
  $handler->display->display_options['filters']['region']['table'] = 'block';
  $handler->display->display_options['filters']['region']['field'] = 'region';
  $handler->display->display_options['filters']['region']['operator'] = 'contains';
  $handler->display->display_options['filters']['region']['group'] = 1;
  $handler->display->display_options['filters']['region']['exposed'] = TRUE;
  $handler->display->display_options['filters']['region']['expose']['operator_id'] = 'region_op';
  $handler->display->display_options['filters']['region']['expose']['label'] = 'Block region';
  $handler->display->display_options['filters']['region']['expose']['operator'] = 'region_op';
  $handler->display->display_options['filters']['region']['expose']['identifier'] = 'region';
  $handler->display->display_options['filters']['region']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    11 => 0,
    16 => 0,
    21 => 0,
    26 => 0,
    31 => 0,
    36 => 0,
    41 => 0,
    46 => 0,
    51 => 0,
    56 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['path'] = 'admin/structure/core-blocks';
  $translatables['core_blocks_example'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Bid'),
    t('.'),
    t(','),
    t('Block administrative title'),
    t('Block subject'),
    t('Block module'),
    t('Block delta'),
    t('Block status'),
    t('Block region'),
    t('Block theme'),
    t('Block weight'),
    t('Block operations'),
    t('Block title'),
    t('Page'),
  );


  $views[$views->name] = $view;

  return $views;
}