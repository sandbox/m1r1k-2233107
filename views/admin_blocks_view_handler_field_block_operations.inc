<?php

class admin_blocks_view_handler_field_block_operations extends views_handler_field_entity {
  public function render($block) {

    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['path'] = 'admin/structure/block/manage/' . $block->block_module . '/' . $block->block_delta . '/configure';
    $this->options['alter']['query'] = drupal_get_destination();

    $text = !empty($this->options['text']) ? $this->options['text'] : t('Configure');
    return $text;

  }
}